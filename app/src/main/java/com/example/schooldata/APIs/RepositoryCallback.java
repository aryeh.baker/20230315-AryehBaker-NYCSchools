package com.example.schooldata.APIs;
//callback interface

public interface RepositoryCallback<T> {
    void onComplete(Result<T> result);
}