package com.example.schooldata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProvider;

/**
 * sets up the recyclerview gets data for recyclerview and is the main activity
 */
public class MainActivity extends AppCompatActivity implements LifecycleOwner {

    private RecyclerView recyclerView;
    private SchoolAdapter adapter;
    MainActivity context;
    /**
     * viewmodel
     */
    MainActivityVM vm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        vm = new ViewModelProvider(context).get(MainActivityVM.class);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SchoolAdapter(context);
        recyclerView.setAdapter(adapter);
        /**
         *  data when ready is sent to the recyclerView adapter
         */
         vm.recyclerData.observe(context, data -> adapter.updateData(data));

   }



}