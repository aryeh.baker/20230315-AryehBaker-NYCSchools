package com.example.schooldata.datatypes;

import com.example.schooldata.datatypes.School;
import com.example.schooldata.datatypes.SchoolAverage;

/**
 * a combined school and school average data using field 'dbn' as an index
 */
public class ItemCombinedData {
    public SchoolAverage schoolAverage;
    public School school;

    public ItemCombinedData(School school, SchoolAverage schoolAverage) {
        this.school = school;
        this.schoolAverage = schoolAverage;
    }


}
